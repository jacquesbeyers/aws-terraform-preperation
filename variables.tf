variable "vpc_id" {}
variable "region" {}
variable "environment" {}

variable "stateBucketName" {}
variable "stateBucketEncrypt" {}
variable "stateBucketKey" {}
variable "stateBucketRegion" {}
variable "stateDynamoDB" {}

variable "shared_tags" {
  description = "Shared tags"
  type        = map(string)
  default = {
    "Managed_By" = "Terraform"
  }
}