environment         = "my-environment"
region              = "ap-southeast-2"
vpc_id              = "vpc-0000000000"
stateBucketName     = "my-statebucket"
stateBucketRegion   = "ap-southeast-2"
stateBucketKey      = "terraform.tfstate" 
stateBucketEncrypt  = true
stateDynamoDB       = "my-statedb"