data "terraform_remote_state" "state" {
    backend = "s3"
    config = {
       bucket         = var.stateBucketName
       region         = var.stateBucketRegion
       key            = var.stateBucketKey
       encrypt        = var.stateBucketEncrypt
       dynamodb_table = var.stateDynamoDB
    }
}