provider "aws" {
  region           = var.region
}

terraform {
  required_version = ">= 0.13"

  required_providers {
    aws            = ">=3.0"
  }
}