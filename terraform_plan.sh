#!/bin/bash

source build_scripts/Variables

terraform init
terraform plan -var-file=env/$ENVIRONMENT.tfvars -out=$TERRAFORM_PLAN
