#!/bin/bash

source build_scripts/Variables

aws s3api create-bucket --bucket $BUCKET_NAME \
    --region $AWS_DEFAULT_REGION \
    --create-bucket-configuration \
    LocationConstraint=$AWS_DEFAULT_REGION

aws s3api put-bucket-encryption \
    --bucket $BUCKET_NAME \
    --server-side-encryption-configuration={\"Rules\":[{\"ApplyServerSideEncryptionByDefault\":{\"SSEAlgorithm\":\"AES256\"}}]}

aws s3api put-bucket-versioning --bucket $BUCKET_NAME --versioning-configuration Status=Enabled

aws s3api delete-public-access-block --bucket $BUCKET_NAME
aws --profile=cloudops-sandbox s3api put-public-access-block --bucket $BUCKET_NAME --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
