#!/bin/bash

source build_scripts/Variables

aws --profile=$PROFILE iam create-user --user-name $TERRAFORM_USER

aws --profile=$PROFILE iam attach-user-policy --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess --user-name $TERRAFORM_USER
aws --profile=$PROFILE iam attach-user-policy --policy-arn arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess --user-name $TERRAFORM_USER

AWS_ACCOUNT_NUMBER=`aws --profile=$PROFILE sts get-caller-identity | grep "\"Account\":.*$" | cut -d ":" -f2 | tr -d -c 0-9`

cat <<EOF> "$ENVIRONMENT-$TERRAFORM_USER-policy.json"
{
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::$AWS_ACCOUNT_NUMBER:user/$TERRAFORM_USER"
            },
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::$BUCKET_NAME"
        }
    ]
}
EOF

aws s3api put-bucket-policy --bucket $BUCKET_NAME --policy file://$ENVIRONMENT-$TERRAFORM_USER-policy.json

rm -f ./$ENVIRONMENT-$TERRAFORM_USER-policy.json
