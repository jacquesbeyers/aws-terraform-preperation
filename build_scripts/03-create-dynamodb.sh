#!/bin/bash

source build_scripts/Variables

cat <<EOF> "$ENVIRONMENT-$TERRAFORM_USER-dynamodb.json"
{
    "TableName": "$BUCKET_NAME",
    "KeySchema": [
      { "AttributeName": "LockID", "KeyType": "HASH" }
    ],
    "AttributeDefinitions": [
      { "AttributeName": "LockID", "AttributeType": "S" }
    ],
    "ProvisionedThroughput": {
      "ReadCapacityUnits": 20,
      "WriteCapacityUnits": 20
    }
}
EOF

aws --profile=$PROFILE dynamodb create-table --cli-input-json file://$ENVIRONMENT-$TERRAFORM_USER-dynamodb.json --tags=Key=Name,Value="DynamoDB for Terraform state file locking"

rm -f ./$ENVIRONMENT-$TERRAFORM_USER-dynamodb.json